Feature: Product Search Feature

  Scenario Outline: Product Search using Searchbox
    Given User is on the Home page
    When User enters <product> in search box
    And clicks on Search button
    Then Products which are related to Search <product> should display
    Then quit the browser

    Examples: 
      | product |
      | Dress   |
      | shirt   |
