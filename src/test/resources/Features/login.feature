Feature: Login Feature

  Scenario Outline: Login with valid Credentials
    Given User is on the authentication page
    When User enters <emailAddress> and <password>
    And clicks on SignIN button
    Then user is navigated to HomePage
    And close the browser

    Examples: 
      | emailAddress          | password  |
      | meda.harish@gmail.com | Admin@123 |
