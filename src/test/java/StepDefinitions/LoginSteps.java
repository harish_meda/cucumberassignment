package StepDefinitions;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class LoginSteps {
	
	WebDriver driver=null;
	
	@Given("User is on the authentication page")
	public void user_is_on_the_authentication_page() {
		System.out.println("1");
		
		System.setProperty("webdriver.chrome.driver", "C:/Users/mharish/new-workspace/CucumberAssignment/src/test/resources/Drivers/chromedriver.exe");
		
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		prefs.put("credentials_enable_service", false);
		prefs.put("profile.password_manager_enabled", false);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		options.addArguments("--disable-extensions");
		options.addArguments("--disable-infobars");
		driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.navigate().to("http://automationpractice.com/index.php?controller=authentication&back=my-account");

		
	  
	}

	
	
	@When("^User enters (.*) and (.*)$")
	public void user_enters_emailAddress_and_password(String emailAddress,String password) {
		driver.findElement(By.id("email")).sendKeys(emailAddress);
		driver.findElement(By.id("passwd")).sendKeys(password);
		
	}
	@When("clicks on SignIN button")
	public void clicks_on_SignIN_button() {
		driver.findElement(By.xpath("//p[@class='submit']//span[1]")).click();
	  
	}

	@Then("user is navigated to HomePage")
	public void user_is_navigated_to_HomePage() {
		String pageTitle=driver.getTitle();
		System.out.println(pageTitle);
		Assert.assertEquals(pageTitle, "My account - My Store");
	
	   
	}
	
	@Then("close the browser")
	public void close_the_browser() {
	    driver.quit();
	}


}
