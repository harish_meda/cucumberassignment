package StepDefinitions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class AutomationPractise {

	WebDriver driver = null;

	public void userLogin() {
		System.setProperty("webdriver.chrome.driver",
				"C:/Users/mharish/new-workspace/CucumberAssignment/src/test/resources/Drivers/chromedriver.exe");

		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		prefs.put("credentials_enable_service", false);
		prefs.put("profile.password_manager_enabled", false);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		options.addArguments("--disable-extensions");
		options.addArguments("--disable-infobars");
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.navigate().to("http://automationpractice.com/index.php");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.findElement(By.id("search_query_top")).sendKeys("dress");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//button[@name='submit_search']")).click();



		List<WebElement> searchedProducts = driver.findElements(By.xpath("//a[@class='product-name']"));
		System.out.println(searchedProducts.size());

		for (WebElement products : searchedProducts) {

			System.out.println(products.getText());
		}

	}

	public void quitBrowser() {

		driver.quit();
	}

	public static void main(String[] args) {

		AutomationPractise ap = new AutomationPractise();
		ap.userLogin();
		//ap.quitBrowser();

	}

}
