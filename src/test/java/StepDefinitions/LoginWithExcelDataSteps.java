package StepDefinitions;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.PKGlobal.CucumberAssignment.ExcelUtil;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class LoginWithExcelDataSteps {
	
	     
	  
	WebDriver driver=null;
	
	
	
	@Given("User is on the sign in page")
	public void user_is_on_the_sign_in_page() {
		
       System.out.println("1");
		
		System.setProperty("webdriver.chrome.driver", "C:/Users/mharish/new-workspace/CucumberAssignment/src/test/resources/Drivers/chromedriver.exe");
		
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		prefs.put("credentials_enable_service", false);
		prefs.put("profile.password_manager_enabled", false);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		options.addArguments("--disable-extensions");
		options.addArguments("--disable-infobars");
		driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.navigate().to("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	   
	}

	@When("User types emailaddress and password from excel")
	public void user_types_and_from_excel_data() {
		
		
		try {
			String emailAddress = ExcelUtil.getCellData(0, 1);
			
			String password = ExcelUtil.getCellData(0, 2);
			
			driver.findElement(By.id("email")).clear();
			driver.findElement(By.id("email")).sendKeys(emailAddress);
			driver.findElement(By.id("passwd")).clear();
			driver.findElement(By.id("passwd")).sendKeys(password);
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		
	}

	@When("click on SignIN button")
	public void click_on_SignIN_button() {
		driver.findElement(By.xpath("//p[@class='submit']//span[1]")).click();
	}

	@Then("HomePage is opened")
	public void homepage_is_opened() {
		String pageTitle=driver.getTitle();
		System.out.println(pageTitle);
		Assert.assertEquals(pageTitle, "Login - My Store");
	}
	
	
}
