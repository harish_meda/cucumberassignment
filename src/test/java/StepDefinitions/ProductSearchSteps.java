package StepDefinitions;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class ProductSearchSteps {
	
	WebDriver driver=null;
	
	
	@Before
	public void userLogin() {
		
        System.setProperty("webdriver.chrome.driver", "C:/Users/mharish/new-workspace/CucumberAssignment/src/test/resources/Drivers/chromedriver.exe");
		
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		prefs.put("credentials_enable_service", false);
		prefs.put("profile.password_manager_enabled", false);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		options.addArguments("--disable-extensions");
		options.addArguments("--disable-infobars");
		driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.navigate().to("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		driver.findElement(By.id("email")).sendKeys("meda.harish@gmail.com");
		driver.findElement(By.id("passwd")).sendKeys("Admin@123");
		driver.findElement(By.xpath("//p[@class='submit']//span[1]")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		
	}
	
	
	
	@Given("User is on the Home page")
	public void user_is_on_the_Home_page() {
		String pageTitle=driver.getTitle();
		System.out.println(pageTitle);
		Assert.assertEquals(pageTitle, "My account - My Store");
	}

	@When("^User enters (.*) in search box$")
	public void user_enters_Dress_in_search_box(String product) {
	    driver.findElement(By.id("search_query_top")).sendKeys(product);
	    
	    
	}

	@When("clicks on Search button")
	public void clicks_on_Search_button() {
		driver.findElement(By.xpath("//button[@name='submit_search']")).click();
	}

	@Then("^Products which are related to Search (.*) should display$")
	public void products_which_are_related_to_Search_keyword_should_display(String productCategory) {
		
		if(productCategory=="dress") {
			
			String product=driver.findElement(By.xpath("//span[@class='lighter']")).getText();
			Assert.assertEquals(product, "dress");
		}
			
			else if(productCategory=="shirt") {
				String product=driver.findElement(By.xpath("//span[@class='lighter']")).getText();
				Assert.assertEquals(product, "shirt");
				
				
			}
				
				
			}
	
	
	@Then("quit the browser")
	public void close_the_browser() {
	    driver.quit();
	}

			
		
	    
	}

	


