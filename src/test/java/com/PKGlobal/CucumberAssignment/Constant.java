package com.PKGlobal.CucumberAssignment;


import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Constant {
	
	
	
	public static Path currentRelativePath = Paths.get("");

	public static final String Path_TestData = currentRelativePath.toAbsolutePath().toString() + File.separator
			+ "Testdata" + File.separator;

	public static final String File_TestData = "TestData.xlsx";

}
